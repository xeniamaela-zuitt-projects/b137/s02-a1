package b137.abano.s02a1;
import java.util.Scanner;

public class LeapYearCalculator {
    public static void main(String[] args) {
        System.out.println("Leap Year Calculator\n");

        /*Scanner appScanner = new Scanner(System.in);
        System.out.println("What is your name?\n");
        String firstName = appScanner.nextLine();
        System.out.println("hello, " + firstName);*/

        //Activity create a program that checks if a year is a leap year or not.

        Scanner appScanner = new Scanner(System.in);
        System.out.println("Enter year:\n");
        int year=appScanner.nextInt();

        if (year % 4 == 0) {

            if (year%100 == 0) {

                if (year % 400 == 0 ) {
                    System.out.println(year + " is a leap year.");
                } else {
                    System.out.println(year + " is not a leap year.");
                }
            } else {
                System.out.println(year + " is a leap year.");
            }

        } else {
            System.out.println(year + " is not a leap year.");
        }
    }
}
